import os, sys
import pandas as pd
import numpy as np
import os.path as path
import math
from datetime import datetime, time


def dist_calc(a,b):
    delta_x2 = (a[0]-a[1])**2 
    delta_y2 = (b[0]-b[1])**2
    distance = math.sqrt(delta_x2 + delta_y2)
    return distance

def date_calc(a,b):
    delta_time = b - a
    return delta_time



# Mise en mï¿½moire des chemins d'accï¿½s aux fichiers
repertoire = path.abspath(path.join(__file__ ,"..\..\.."))# Changes
fichier_in = "TP1\CSV_in\GPS.csv"

fichier_in_name = os.path.join(repertoire, fichier_in)
gps = os.path.abspath(os.path.realpath(fichier_in_name))


# Vï¿½rifier si le fichier csv existe.  Sinon, aviser l'usager et sortie!
if not os.path.exists(gps):
    msg = "Le fichier %s est introuvable!" %(gps)
    sys.exit(msg)

# Spï¿½cifier le fichier de sortie
fichier_out = "CSV_out\Stats.csv"
fichier_prope = os.path.join(repertoire, fichier_out)

# Spï¿½cifier le fichier prope (intermediaire)
#fichier_prope = os.path.join(repertoire, "GPS_prope.csv")

# My code
df = pd.read_csv(gps, sep=';')

#retriving the header
header = list(df.columns.values)

headerExpected =['OBJECTID', 'DATE_SAISIE_UTC', 'ID_VEH', 'ID_RAWPACKETDATA', 'ID_GPS', 'LONGITUDE', 'LATITUDE', 'DATE_SAISIE', 'NBR_SAT', 'ORIENTATION', 'VITESSE', 'FIX_GPS', 'HDOP', 'DATA_TRX', 'SEC_ARRET', 'ADRESSE_IP', 'ID_OPERATION', 'X', 'Y']


notTheSame = len([i for i, j in zip(header, headerExpected) if i != j])

if notTheSame != 0:
    print "Le fichier %s est introuvable!" 
    msg = "Le fichier %s est introuvable!" %(gps)
    sys.exit(msg)


ids_Cars = []
dic_soma_dist = {}
dic_media_dist = {}
dic_pontos_memo = {}

# Cria uma lista com todos os IDS dos veiculos (ids unicos)
count = 0
for row in df.iterrows():
    if row[1][2] not in ids_Cars:
        ids_Cars.append(row[1][2])
    
# itera sobre os IDs
for id in ids_Cars:
    # Retira apenas as linhas cujas rotas se refiram ao veiculo do ID.
    df_temp = df.loc[df['ID_VEH'] == id]
    #OBJECTID = list(df_temp.loc[:,'OBJECTID'])

    # Cria uma lista para todas as coordenadas X e Y
    X = list(df_temp.loc[:,'X'])
    Y = list(df_temp.loc[:,'Y'])

    # Cria uma lista para as datas
    date_points = list(df_temp.loc[:,'DATE_SAISIE'])

    # Verifica quantos pontos tem
    n = len(X)
    sum = 0
    date_ini = 0
    date_end = 0

    # Itero sobre todos os pontos daquele veiculo calculando a distancia percorrida e armazenando em um dicionario cuja chave Ã© o ID e o valor Ã© a soma de todas as rotas.
    # Estou supondo que a ordem com que aparecem seja a ordem dos pontos para calcular a distÃ¢ncia entre o ponto a e o b.
    for i in range(n-1):
        if i == 0:
            date_ini = datetime.strptime(date_points[i], '%Y-%m-%d %H:%M:%S')
        if i == n-2:
            date_end = datetime.strptime(date_points[i], '%Y-%m-%d %H:%M:%S')
        a = [float(X[i].replace(',','.')),float(Y[i].replace(',','.'))]
        b = [float(X[i+1].replace(',','.')),float(Y[i+1].replace(',','.'))]
        dist_calc(a,b)
        sum  = sum + dist_calc(a,b)
    
    
    time = date_end - date_ini
    dic_soma_dist[id] = sum
    dic_media_dist[id] = sum/n
    dic_pontos_memo[id] = n
    # Tem que transformar o tempo em horas e dividir o espaco pelo tempo
    #print dic_soma_dist[id]/time
     



